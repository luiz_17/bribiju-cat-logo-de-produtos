22 - Efetuar Login
================================================================================================
1.1 Contexto
	Ator deseja logar no sistema.
1.2 Ator primário
	Internauta.
1.3 Pré-Condição
	Ter realizado o cadastro.
1.4 Pós-Condição
	A sessão é iniciada.
1.5 Cenário Principal
	1 – [IN] Ator fornece seu e-mail e senha para o sistema.
	2 – Sistema valida os dados.
	3 – Sistema redireciona o ator para página inicial.
1.6 Exceções
	2a – Ator ou senha fornecidos incorretos ou inexistente.
		2a.1 – [OUT] Sistema informa erro e solicita reenvio de dados.