17 - Criar Conta
================================================================================================
1.1 Contexto
	Ator deseja criar uma conta no sistema.
1.2 Ator primário
	Internauta.
1.3 Pré-Condição
1.4 Pós-Condição
	Internauta se torna um usuário novo no sistema.
1.5 Cenário Principal
	1 – [IN] Ator solicita criar conta.
	2 – [OUT] Sistema retorna ao ator a tela de criar conta.
	3 – [IN] Ator preenche as informações e envia:
		3.1 – Nome completo.		Texto - obrigatório
		3.2 – E-mail				Alfanumérico - obrigatório
		3.3 – Telefone				Numérico
		3.4 – Senha					Alfanumérico - obrigatório
		3.5 – Confirmação de senha	Alfanumérico - obrigatório
		3.6 – Avatar				Imagem – 1MB
	4 – Sistema valida as informações.
	5 – [OUT] Sistema retorna uma mensagem de sucesso “Cadastro efetuado com sucesso!”.
	6 – Sistema redireciona o ator para página de administração do usuário.
1.6 Exceções
	4a - Algum campo não preenchido.
		4a.1 - [OUT] Sistema emite mensagem de erro, “É Necessário o preenchimento deste campo”.
		4a.2 - [OUT] Sistema retorna ao passo 3 do cenário principal.
	4b - Campo preenchido de forma incorreta.
		4b.1- [OUT] Sistema emite mensagem de erro, “Campo preenchido de forma incorreta”.
		4b.2 - [OUT] Sistema retorna ao passo 3 do cenário principal.
	4c – Campos “3.4 - Senha” e “3.5 - Confirmação de senha” divergentes.
		4c.1 - [OUT] Sistema informa que os campos "Senha" e "Confirmação" não conferem.
		4c.2 - [OUT] Sistema retorna ao passo 3.4 do cenário principal (exibe formulário com os dados).
	4d - Dados chave em conflito com outros usuários.
		4d.1 - [OUT] Sistema informa que os campos “3.2- 
	 	e-mail já está cadastrado para outro usuário”.
		4d.2 - [OUT] Sistema emite mensagem de erro, “Este e-mail já foi cadastrado”, e sistema retorna ao passo 3.2 do cenário principal (exibe formulário).