6 – Excluir Produto
================================================================================================
1.1 Contexto:
	Ator deseja excluir produto cadastrado no sistema.
1.2 Ator Primário:
	Administrador.
1.3 Pré-Condição:
	Ator ter efetuado a rotina Consultar Produto.
1.4 Pós-Condição:
	Produto Excluído com Sucesso.
1.5 Cenário Principal:
	1 - [IN] Ator solicita Excluir Produto cadastrado no sistema.
	2 – [OUT] Sistema emite mensagem pedindo confirmação, “Deseja Excluir este produto?”.
	3 – [IN] Ator confirma 
	4 – Sistema exclui o produto do banco de dados.
	5 – [OUT] Sistema retorna mensagem de sucesso, “Produto Excluído!”. 
1.6 Exceções:
	3a – Ator não confirma a exclusão do produto.
			3a.1 – Sistema retorna à rotina de Consulta do Produto. (não inativar processo).