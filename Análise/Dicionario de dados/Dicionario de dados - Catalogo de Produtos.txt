Dicionário de Dados
================================================================================================

Caso de uso 1 - Cadastrar Usuário
	Nome completo - Nome completo do ator - Texto - obrigatório
	E-mail - E-mail do ator - Alfanumérico - obrigatório
	Nível de Acesso - Define o nível de acesso da conta a ser cadastrada - [] Usuário ou [] Administrador - obrigatório
	Telefone - Telefone do ator - Numérico
	Senha - Senha para acessar o sistema - Alfanumérico - obrigatório
	Confirmação de senha - Senha de confirmação - Alfanumérico - obrigatório
	Avatar - Imagem de no máximo 1MB - PNG e JPEG.

Caso de uso 4 - Cadastrar Produto
	Nome do produto - Nome completo do produto - Texto – obrigatório		
	Foto do produto	- Imagem ilustrativa do produto com tamanho máximo de 1MB - obrigatório - PNG e JPEG
	Categoria - Campo para selecionar as categorias relacionadas ao produto - Dropbox - obrigatório
	Preço - Preço do produto - Numérico - obrigatório
	Descrição do produto - Descrição do produto a ser cadastrado - Texto – obrigatório

Caso de uso 5 - Consultar Produto
	Nome do produto - Nome completo do produto - Texto – obrigatório
	Categoria - Campo para selecionar as categorias relacionadas ao produto - Dropbox - obrigatório

Caso de uso 7 - Alterar Produto
	Nome do produto - Nome completo do produto - Texto – obrigatório		
	Foto do produto	- Imagem ilustrativa do produto com tamanho máximo de 1MB - obrigatório - PNG e JPEG
	Categoria - Campo para selecionar as categorias relacionadas ao produto - Dropbox - obrigatório
	Preço - Preço do produto - Numérico - obrigatório
	Descrição do produto - Descrição do produto a ser cadastrado - Texto – obrigatório

Caso de uso 9 - Cadastrar Categoria
	Nome da categoria - Nome que identifica a categoria - Texto - obrigatório
	Descrição - texto descrevendo a categoria - Texto

Caso de uso 10 - Consultar Categoria
	Nome da categoria - Nome que identifica a categoria - Texto - obrigatório

Caso de uso 11 - Alterar Categoria
	Nome da categoria - Nome que identifica a categoria - Texto - obrigatório
	Descrição - texto descrevendo a categoria - Texto

Caso de uso 15 - Avaliar Produto
	1 estrela -  Voto do ator - Inteiro
	2 estrelas -  Voto do ator - Inteiro
	3 estrelas -  Voto do ator - Inteiro
	4 estrelas -  Voto do ator - Inteiro
 	5 estrelas -  Voto do ator - Inteiro

Caso de uso 16 - Comentar Produto
	Mensagem - Comentário do ator - Texto – obrigatório

Caso de uso 17 - Criar Conta
	Nome completo - Nome completo do ator - Texto - obrigatório
	E-mail - E-mail do ator - Alfanumérico - obrigatório
	Telefone - Telefone do ator - Numérico
	Senha - Senha para acessar o sistema - Alfanumérico - obrigatório
	Confirmação de senha - Senha de confirmação - Alfanumérico - obrigatório
	Avatar - Imagem de no máximo 1MB - PNG e JPEG.

Caso de uso 19 - Alterar Conta
	Nome completo - Nome completo do ator - Texto - obrigatório
	E-mail - E-mail do ator - Alfanumérico - obrigatório
	Telefone - Telefone do ator - Numérico
	Senha - Senha para acessar o sistema - Alfanumérico - obrigatório
	Confirmação de senha - Senha de confirmação - Alfanumérico - obrigatório
	Avatar - Imagem de no máximo 1MB - PNG e JPEG.

Caso de Uso 21 - Entrar em contato
	Nome completo - Nome completo do ator - Texto - obrigatório
	E-mail - E-mail do ator - Alfanumérico - obrigatório
	Mensagem - Mensagem de contato do ator - Texto – obrigatório